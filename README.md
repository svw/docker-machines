# Docker files
A series of base images for my docker mcahines.


## Overview
These images are the basis for my prod (and dev) machines


## Directory naming

Directories are named

XX-<image>[_tag]

where they are sorted based on `XX`, named `${USER}/image` and tagged (if wanted) `tag`.
